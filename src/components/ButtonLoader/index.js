import styles from './styles.module.scss';

const ButtonLoader = () => (
  <div className={styles.ldsRing}>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);

export default ButtonLoader;
