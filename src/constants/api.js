export const API = {
  PRODUCT: {
    PRODUCT_LIST:
      'https://yalantis-react-school-api.yalantis.com/api/v1/products',
    PRODUCT_ITEM:
      'https://yalantis-react-school-api.yalantis.com/api/v1/products/:id',
    FETCH_ORIGINS:
      'https://yalantis-react-school-api.yalantis.com/api/v1/products-origins',
  },
};

export const apiKey =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmdWxsTmFtZSI6IkRlbnlzIE1hcnRzeW5pdWsiLCJpYXQiOjE2MTExNzQwNDIsImV4cCI6MTYxNjM1ODA0Mn0.u1pqG4EN1Of-AcPlQbi7oa7bAN5WcRp36jELJzTM24k';
